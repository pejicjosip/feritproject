import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HPportfolioRoutingModule } from './hpportfolio-routing.module';
import { HPportfolioComponent } from './hpportfolio/hpportfolio.component';


@NgModule({
  declarations: [
    HPportfolioComponent
  ],
  imports: [
    CommonModule,
    HPportfolioRoutingModule
  ]
})
export class HPportfolioModule { }
