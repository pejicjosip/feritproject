import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JPportfolioComponent } from './jpportfolio/jpportfolio.component';

const routes: Routes = [
  {
    path: '',
    component: JPportfolioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JPportfolioRoutingModule { }
