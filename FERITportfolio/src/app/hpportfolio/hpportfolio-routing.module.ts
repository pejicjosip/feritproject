import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HPportfolioComponent } from './hpportfolio/hpportfolio.component';

const routes: Routes = [
  {
    path:'',
    component: HPportfolioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HPportfolioRoutingModule { }
