import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JPportfolioRoutingModule } from './jpportfolio-routing.module';
import { JPportfolioComponent } from './jpportfolio/jpportfolio.component';


@NgModule({
  declarations: [
    JPportfolioComponent
  ],
  imports: [
    CommonModule,
    JPportfolioRoutingModule
  ]
})
export class JPportfolioModule { }
