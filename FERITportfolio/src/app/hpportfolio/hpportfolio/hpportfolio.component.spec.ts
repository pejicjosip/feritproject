import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HPportfolioComponent } from './hpportfolio.component';

describe('HPportfolioComponent', () => {
  let component: HPportfolioComponent;
  let fixture: ComponentFixture<HPportfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HPportfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HPportfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
