import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { HPportfolioModule } from './hpportfolio/hpportfolio.module';
import { JPportfolioModule } from './jpportfolio/jpportfolio.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    JPportfolioModule,
    HPportfolioModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
