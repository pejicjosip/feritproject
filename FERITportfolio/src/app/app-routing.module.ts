import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m=> m.HomeModule)
  },
  {
    path: 'HPportfolio',
    loadChildren: () => import('./hpportfolio/hpportfolio.module').then(m=> m.HPportfolioModule)
  },
  {
    path: 'JPportfolio',
    loadChildren: ()=> import('./jpportfolio/jpportfolio.module').then(m=> m.JPportfolioModule)
  },
  {
    path : '**', redirectTo: 'home', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
