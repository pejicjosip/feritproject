import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JPportfolioComponent } from './jpportfolio.component';

describe('JPportfolioComponent', () => {
  let component: JPportfolioComponent;
  let fixture: ComponentFixture<JPportfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JPportfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JPportfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
